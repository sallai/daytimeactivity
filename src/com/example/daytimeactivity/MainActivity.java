package com.example.daytimeactivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final Button b = (Button) findViewById(R.id.button1);
		final TextView tv =  (TextView) findViewById(R.id.textView1);
		
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				new AsyncTask<Void, Void, String>() {

					@Override
					protected String doInBackground(Void... params) {
						Socket s = null;
						BufferedReader br = null;
						try {
							s = new Socket("time.nist.gov", 13);
							br = new BufferedReader(new InputStreamReader(s.getInputStream()));
							
							String line = null;
							
							line = br.readLine(); // this should be empty
							
							line = br.readLine();
							
							return line;
							
						} catch (UnknownHostException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						} finally {
							if (br != null)
								try {
									br.close();
								} catch (IOException e) {}
						}
						
						return "Nothing received";
					}

					@Override
					protected void onPostExecute(String line) {
						tv.setText(line);
					}
					
				}.execute();
				
				
			}
		});
		
	}

}
